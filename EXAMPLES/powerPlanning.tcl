read_lef -lef /apps/potato/TESTS/library/NangateOpenCellLibrary_PDKv1_2_v2008_10.lef -tech also

# creatng a conceptual design  hierarchically 
#createPseudoTopModule -top POC -W 2000 -H 2000
#commit_module -module POC
#edit_module -module POC
#hier2flat --logical --physical
#
read_verilog -v /apps/potato/TESTS/netlists/fifo.vg
elaborate

#set_floorplan_parameters -UTILIZATION 40
set_floorplan_parameters -WIDTH 500 -HEIGHT 500
set_floorplan -force


create_net -type power -name VDD
create_net -type ground -name GND
addPowerRing -offset {0.2,0.2} -spacing 0.5 -width 1 -layerH metal3 -layerV metal2 -nets {VDD,GND}
addPowerRows -width 0.5 -layer metal1 -nets {VDD,GND}
addPowerVias
