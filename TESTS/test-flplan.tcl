read_lef -lef library/NangateOpenCellLibrary_PDKv1_2_v2008_10.lef  -tech also

read_verilog -v netlists/fifo.vg
elaborate

set_floorplan_parameter -UTILIZATION 40
set_floorplan_parameter -WIDTH 50 -HEIGHT 50
set_floorplan -force

create_net -type power -name VDD
create_net -type ground -name GND

addPowerRing -offset {0.2,0.2} -spacing 0.5 -width 1 -layerH metal3 -layerV metal2 -nets {VDD,GND}
addPowerRows -width 0.5 -layer metal1 -nets {VDD,GND}

addPowerVias

write_sdef --via --net
write_def -output fifo.def --overwrite

#place_graywolf
