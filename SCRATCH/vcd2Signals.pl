#!/usr/bin/perl -w 

use Verilog::VCD qw(:all);
my @signals = list_sigs($ARGV[0]);

foreach (@signals) {
  print "$_\n";
}
