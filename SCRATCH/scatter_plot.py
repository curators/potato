import matplotlib.pyplot as plt
import csv
import sys
arg1 = sys.argv[1]
file = open(arg1, "r" )
data = list(csv.reader(file, delimiter=","))
file.close()

fig, ax = plt.subplots()

manhattanLen = [ float(col[1]) for col in data ]
actualLen = [ float(col[0]) for col in data ]

ax.scatter(manhattanLen, actualLen)
ax.legend()
ax.set_xlabel('manhattan length')
ax.set_ylabel('actual length')
ax.set_title(arg1)
plt.show()
