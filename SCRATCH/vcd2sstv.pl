#!/usr/bin/perl 
use Benchmark;
my $t0 = new Benchmark;
my $noOfArguments = @ARGV; 

if($ARGV[0] eq "-h" || $ARGV[0] eq "-help" || $ARGV[0] eq "-HELP"){
  print "Usage : vcd2sstvFast\n";
  print "      : -vcd <fileName>\n";
  print "      : -output<fileName>\n";
}else {
  my $vcd_file = "";
  my $output = "";
  for(my $i =0; $i<$noOfArguments;$i++){
    if($ARGV[$i] eq "-vcd"){$vcd_file = $ARGV[$i+1];}
    if($ARGV[$i] eq "-output"){$output = $ARGV[$i+1];}
  }#for
  if(( -e $vcd_file) && (-r $vcd_file)){
    print "INFO : 001 : $vcd_file file exists and is readable!\n";
  }else{
    print "INFO : 001 : either $vcd_file file does not exists or it is not readable!\n";
    return;
  }
  open(WRITE,">$output");
  open(READ_VCD,$vcd_file);
 my $startProcessSstv = 0 ;
 my $timeStamp = 0 ;
  while(<READ_VCD>){
    chomp();
    if($_ =~ /^\s*\$enddefinitions\b/){ $startProcessSstv =1 ; next ;}
    if($_ =~ /^\s*\$dumpvars\b/){ $startProcessSstv = 1 ; next ; }
    if( $startProcessSstv == 0 ) { next ; }
    if ($_ =~ /^[#](\d+)/) { $timeStamp =  $1; }
    if(($_ =~ /^([01zx])(.+)/i )||($_ =~  /^[b](\S+)\s+(.+)/i)){
      my $value = lc $1;
      my $code  = $2;
      if($_ !~ /^b/){
        print WRITE "$timeStamp $value $code\n"; 
        #print "$timeStamp $value $code\n"; 
      }else {
        print WRITE "$timeStamp $value $code\n"; 
        #print "$timeStamp $value $code\n"; 
        }
    }
  }#while
  close(WRITE);
}#else
my $t1 = new Benchmark;
my $td = timediff($t1, $t0);
print "Command vcd2sstvFast took:",timestr($td),"\n";
