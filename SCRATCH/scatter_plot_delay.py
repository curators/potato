import matplotlib.pyplot as plt
import csv
import sys
arg1 = sys.argv[1]
file = open(arg1, "r" )
data = list(csv.reader(file, delimiter=","))
file.close()

fig, ax = plt.subplots()

delay = [ float(col[1]) for col in data ]
Id = [ float(col[0]) for col in data ]

ax.scatter(Id, delay)
ax.legend()
ax.set_xlabel('path')
ax.set_ylabel('delay')
ax.set_title(arg1)
plt.show()
