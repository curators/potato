#!/usr/bin/perl -w 
use Benchmark;
my $t0 = new Benchmark;

use Verilog::VCD qw(:all);
my $vcd = parse_vcd($ARGV[0], {only_sigs => 1});

print "Done ...\n";
open(WRITE,">NameMap.txt");

foreach $codeName ( keys %{$vcd} ) {
   
         $refNode = ${$vcd}{$codeName}{nets};
         foreach $n (@{$refNode}) { 
                         $hierName = ${$n}{hier};
                         $hierName =~ s/\./\//g;
                         $hierName =~ s/tb\/u_mcu_top_gasket\/u_mcu_top\///g;
                         $netName = ${$n}{name};
                         $fullName = $hierName."/".$netName;

                         print WRITE "$codeName $fullName \n";
                                  }
        
                                   }#foreach keys of vcd

close(WRITE);

my $t1 = new Benchmark;
my $td = timediff($t1, $t0);
print "Command vcd2NameMap.pl took:",timestr($td),"\n";
