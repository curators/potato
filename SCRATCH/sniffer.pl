

#use File::stat qw/:stat/;
use File::stat ;

my $DB_FILE = "/tmp/sniff_files.db";
my $WHITE_FILE = "/tmp/snifferWhiltelist.db";
my $NEW_FILES = "/tmp/t";
my %FILE_HASH = ();

open(WRITE,">$NEW_FILES");
#------------- DB for existing files . to check timestamp and if it has changed ---#
print "setting up file DB \n";
open(READ,$DB_FILE);
while(<READ>) {
chomp();
next if ( $_ =~ /^starting at/);
my ($fileName,$timeStamp) = (split(/\s+/,$_))[1,3];
$FILE_HASH{$fileName} = $timeStamp;
}
close(READ);



$base_path = $ARGV[0];



sub is_file_rtl {
my $FILE = $_[0];
my $RANK = 0 ;
open(READ,"$FILE");
my $RTL_PROB_A = 0 ;
my $RTL_PROB_B = 0 ;
my $RTL_PROB_C = 0 ;
my $RTL_PROB_D = 0 ;
my $RTL_PROB_E = 0 ;
my $RTL_PROB_F = 0 ;
while(<READ>) {
chomp();
      if($_=~ /module\b/) {  $RTL_PROB_A++; }
      if($_=~ /endmodule\b/) {  $RTL_PROB_A++; }
      if($_=~ /always\b/) {  $RTL_PROB_B++; }
      if($_=~ /assign\b/) {  $RTL_PROB_C++; }
      if($_=~ /begin\b/) {  $RTL_PROB_D++; }
      if($_=~ /end\b/) {  $RTL_PROB_E++; }
      if($_=~ /param\b/) {  $RTL_PROB_F++; }
#      print "RTL_PROB = $RTL_PROB_A $RTL_PROB_B $RTL_PROB_C $RTL_PROB_D $RTL_PROB_E $RTL_PROB_F\n";
              }#while
              if ( $RTL_PROB_A > 0 ) {
$RANK = ( ($RTL_PROB_A+$RTL_PROB_B+$RTL_PROB_C+$RTL_PROB_D+$RTL_PROB_E+$RTL_PROB_F) / $RTL_PROB_A );
                                     }
print "RANK $RANK\n";
if($RTL_PROB > 2 ) {
return(1);
} else { return 0; }

}#sub is_file_rtl

process_files ($base_path);

# Accepts one argument: the full path to a directory.
# Returns: A list of files that reside in that path.
sub process_files {
    my $path = shift;
    #print "starting at $path\n";

    opendir (DIR, $path)
        or die "Unable to open $path: $!";

    # We are just chaining the grep and map from
    # the previous example.
    # You'll see this often, so pay attention ;)
    # This is the same as:
    # LIST = map(EXP, grep(EXP, readdir()))
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);

    for (@files) {
        if (-d $_ && ! -l $_ && $_ !~ /\.git/ ) {
            # Add all of the new files from this directory
            # (and its subdirectories, and so on... if any)
            push @files, process_files ($_);

        } else {
            # Do whatever you want here =) .. if anything.
            next if ( $_ =~ /\.xlsx/) ;
            next if ( $_ eq "") ;

        $stats = stat($_);
        my $time = $stats->[9];
        my $user = $stats->[4];
        if ( exists $FILE_HASH{$_} ) {
                    if ( $FILE_HASH{$_} eq $time) {
                                                  next;
                                                  } else {
                                                     print "$_ changed \n";
                                                  }
                                     } else {
                    print WRITE "file $_ $user $time\n";
                                    print "check if $_ is an RTL file \n";
                                    $FLAG_RTL = is_file_rtl($_);
                                    if ( $RTL_FLAG ==1) { print "Alert! : new RTL file found\n"; } else { print "new file is not RTL \n"; }  
                                     }
        }
    }
    # NOTE: we're returning the list of files
    return @files;
          
}
close (WRITE);
